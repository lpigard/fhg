#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "openacc.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

int index(int x, int y, int z, int Nx, int Ny, int Nz)
{
    return z + Nz * (y + Ny * x);
}

int main(int argc, char *argv[])
{
    const int argc_valid = 15;

    if(argc != argc_valid)
    {
        printf("USAGE: ./run Lx Ly Lz Nx Ny Nz dt tend dt_ana name chiN0 eps T gamma (%d / %d)\n", argc, argc_valid);
        return -1;
    }

    const float Lx = atof(argv[1]);
    const float Ly = atof(argv[2]);
    const float Lz = atof(argv[3]);
    const int Nx = atoi(argv[4]);
    const int Ny = atoi(argv[5]);
    const int Nz = atoi(argv[6]);
    const float dt = atof(argv[7]);
    const float tend = atof(argv[8]);
    const int dt_ana = atoi(argv[9]);
    const float chiN0 = atof(argv[11]);
    const float eps = atof(argv[12]);
    const float T = atof(argv[13]);
    const float gamma = atof(argv[14]);

    printf("Lx=%lg Ly=%lg Lz=%lg Nx=%d Ny=%d Nz=%d dt=%lg tend=%lg dt_ana=%d name=%s chiN0=%lg eps=%lg T=%lg gamma=%lg\n", Lx, Ly, Lz, Nx, Ny, Nz, dt, tend, dt_ana, argv[10], chiN0, eps, T, gamma);

    const int n = Nx * Ny * Nz;

    float * m = (float*)malloc(n * sizeof(float));
    float * mu = (float*)malloc(n * sizeof(float));
    float * m_new = (float*)malloc(n * sizeof(float));
    float * j_x = (float*)malloc(n * sizeof(float));
    float * j_y = (float*)malloc(n * sizeof(float));
    float * j_z = (float*)malloc(n * sizeof(float));

    const float dx = Lx / Nx;
    const float dy = Ly / Ny;
    const float dz = Lz / Nz;

    srand(time(NULL));

    for(int i = 0; i < n; i++)
    {
        m[i] = 0.01 * (rand() / (float) RAND_MAX - 0.5);
        // printf("%.3e\n", m[i]);
    }

    // #pragma acc enter data copyin(m[0:n],m_new[0:n],mu[0:n])
    #pragma acc enter data copyin(m[0:n],m_new[0:n],mu[0:n],j_x[0:n],j_y[0:n],j_z[0:n])

    const int T_int = (int)(T / dt);
    const int hT_int = (int)(gamma * T / dt / 2);
    printf("%d %d\n", T_int, hT_int);

    for(long t = 0; t * dt < tend; t++)
    {
        if(t % dt_ana == 0)
        {
            printf("t=%lg;\n", t * dt);

            char fname[1024];
            sprintf(fname, "%s_m_%lg.dat", argv[10], t * dt);
            FILE *f = fopen(fname, "w");

            #pragma acc update host(m[0:n])

            for(int i = 0; i < n; i ++)
            {
                fprintf(f, "%g ", m[i]);
            }

            fclose(f);
        }

        // const float chiN = 1 + eps * sin(2 * M_PI * t * dt / T);
        const float chiN = t % T_int < hT_int / 2 ? 1 + eps : 1 - eps;

        #pragma acc parallel loop collapse(3)
        for(int x = 0; x < Nx; x++)
        {
            for(int y = 0; y < Ny; y++)
            {
                for(int z = 0; z < Nz; z++)
                {
                    const int i = index(x, y, z, Nx, Ny, Nz);
                    const float tmp = 1 / (1 - m[i] * m[i]);
                    const float mp1_x = m[index((x + 1) % Nx,   y, z, Nx, Ny, Nz)];
                    const float mp1_y = m[index(x,              (y + 1) % Ny,   z, Nx, Ny, Nz)];
                    const float mp1_z = m[index(x,              y,              (z + 1) % Nz, Nx, Ny, Nz)];
                    const float mm1_x = m[index((x - 1 + Nx) % Nx,  y, z, Nx, Ny, Nz)];
                    const float mm1_y = m[index(x,                  (y - 1 + Ny) % Ny,  z, Nx, Ny, Nz)];
                    const float mm1_z = m[index(x,                  y,                  (z - 1 + Nz) % Nz, Nx, Ny, Nz)];
                    const float dm_dx = (mp1_x - mm1_x) / (2 * dx);
                    const float dm_dy = (mp1_y - mm1_y) / (2 * dy);
                    const float dm_dz = (mp1_z - mm1_z) / (2 * dz);
                    const float d2m_dx2 = (mp1_x + mm1_x - 2 * m[i]) / (dx * dx);
                    const float d2m_dy2 = (mp1_y + mm1_y - 2 * m[i]) / (dy * dy);
                    const float d2m_dz2 = (mp1_z + mm1_z - 2 * m[i]) / (dz * dz);

                    mu[i] = log((1 + m[i]) / (1 - m[i])) / chiN0 - chiN * m[i] - (d2m_dx2 + d2m_dy2 + d2m_dz2) * tmp - m[i] * (dm_dx * dm_dx + dm_dy * dm_dy + dm_dz * dm_dz) * tmp * tmp;;
                }
            }
        }

        // #pragma acc parallel loop collapse(3)
        // for(int x = 0; x < Nx; x++)
        // {
        //     for(int y = 0; y < Ny; y++)
        //     {
        //         for(int z = 0; z < Nz; z++)
        //         {
        //             const int i = index(x, y, z, Nx, Ny, Nz);
        //             const float mp1_x = m[index((x + 1) % Nx,   y, z, Nx, Ny, Nz)];
        //             const float mp1_y = m[index(x,              (y + 1) % Ny,   z, Nx, Ny, Nz)];
        //             const float mp1_z = m[index(x,              y,              (z + 1) % Nz, Nx, Ny, Nz)];
        //             const float mm1_x = m[index((x - 1 + Nx) % Nx,  y, z, Nx, Ny, Nz)];
        //             const float mm1_y = m[index(x,                  (y - 1 + Ny) % Ny,  z, Nx, Ny, Nz)];
        //             const float mm1_z = m[index(x,                  y,                  (z - 1 + Nz) % Nz, Nx, Ny, Nz)];
        //             const float dm_dx = (mp1_x - mm1_x) / (2 * dx);
        //             const float dm_dy = (mp1_y - mm1_y) / (2 * dy);
        //             const float dm_dz = (mp1_z - mm1_z) / (2 * dz);
        //
        //             const float mup1_x = mu[index((x + 1) % Nx,   y, z, Nx, Ny, Nz)];
        //             const float mup1_y = mu[index(x,              (y + 1) % Ny,   z, Nx, Ny, Nz)];
        //             const float mup1_z = mu[index(x,              y,              (z + 1) % Nz, Nx, Ny, Nz)];
        //             const float mum1_x = mu[index((x - 1 + Nx) % Nx,  y, z, Nx, Ny, Nz)];
        //             const float mum1_y = mu[index(x,                  (y - 1 + Ny) % Ny,  z, Nx, Ny, Nz)];
        //             const float mum1_z = mu[index(x,                  y,                  (z - 1 + Nz) % Nz, Nx, Ny, Nz)];
        //             const float dmu_dx = (mup1_x - mum1_x) / (2 * dx);
        //             const float dmu_dy = (mup1_y - mum1_y) / (2 * dy);
        //             const float dmu_dz = (mup1_z - mum1_z) / (2 * dz);
        //             const float d2mu_dx2 = (mup1_x + mum1_x - 2 * mu[i]) / (dx * dx);
        //             const float d2mu_dy2 = (mup1_y + mum1_y - 2 * mu[i]) / (dy * dy);
        //             const float d2mu_dz2 = (mup1_z + mum1_z - 2 * mu[i]) / (dz * dz);
        //
        //             m_new[i] = m[i] + dt * ((1 - m[i] * m[i]) * (d2mu_dx2 + d2mu_dy2 + d2mu_dz2) - 2 * m[i] * (dm_dx * dmu_dx + dm_dy * dmu_dy + dm_dz * dmu_dz));
        //         }
        //     }
        // }

        #pragma acc parallel loop collapse(3)
        for(int x = 0; x < Nx; x++)
        {
            for(int y = 0; y < Ny; y++)
            {
                for(int z = 0; z < Nz; z++)
                {
                    const int i = index(x, y, z, Nx, Ny, Nz);

                    const float mup1_x = mu[index((x + 1) % Nx,   y, z, Nx, Ny, Nz)];
                    const float mup1_y = mu[index(x,              (y + 1) % Ny,   z, Nx, Ny, Nz)];
                    const float mup1_z = mu[index(x,              y,              (z + 1) % Nz, Nx, Ny, Nz)];
                    const float mum1_x = mu[index((x - 1 + Nx) % Nx,  y, z, Nx, Ny, Nz)];
                    const float mum1_y = mu[index(x,                  (y - 1 + Ny) % Ny,  z, Nx, Ny, Nz)];
                    const float mum1_z = mu[index(x,                  y,                  (z - 1 + Nz) % Nz, Nx, Ny, Nz)];
                    const float dmu_dx = (mup1_x - mum1_x) / (2 * dx);
                    const float dmu_dy = (mup1_y - mum1_y) / (2 * dy);
                    const float dmu_dz = (mup1_z - mum1_z) / (2 * dz);

                    j_x[i] = (1 - m[i] * m[i]) * dmu_dx;
                    j_y[i] = (1 - m[i] * m[i]) * dmu_dy;
                    j_z[i] = (1 - m[i] * m[i]) * dmu_dz;
                }
            }
        }

        #pragma acc parallel loop collapse(3)
        for(int x = 0; x < Nx; x++)
        {
            for(int y = 0; y < Ny; y++)
            {
                for(int z = 0; z < Nz; z++)
                {
                    const int i = index(x, y, z, Nx, Ny, Nz);
                    const float j_xp1_x = j_x[index((x + 1) % Nx,   y, z, Nx, Ny, Nz)];
                    const float j_yp1_y = j_y[index(x,              (y + 1) % Ny,   z, Nx, Ny, Nz)];
                    const float j_zp1_z = j_z[index(x,              y,              (z + 1) % Nz, Nx, Ny, Nz)];
                    const float j_xm1_x = j_x[index((x - 1 + Nx) % Nx,   y, z, Nx, Ny, Nz)];
                    const float j_ym1_y = j_y[index(x,              (y - 1 + Ny) % Ny,   z, Nx, Ny, Nz)];
                    const float j_zm1_z = j_z[index(x,              y,              (z - 1 + Nz) % Nz, Nx, Ny, Nz)];

                    const float dj_x_dx = (j_xp1_x - j_xm1_x) / (2 * dx);
                    const float dj_y_dy = (j_yp1_y - j_ym1_y) / (2 * dy);
                    const float dj_z_dz = (j_zp1_z - j_zm1_z) / (2 * dz);

                    m[i] += dt * (dj_x_dx + dj_y_dy + dj_z_dz);
                }
            }
        }

        // #pragma acc parallel loop
        // for(int i = 0; i < n; i++)
        // {
        //     m[i] = m_new[i];
        // }
    }

    // #pragma acc exit data copyout(m[0:n],m_new[0:n],mu[0:n])
    #pragma acc exit data copyout(m[0:n],m_new[0:n],mu[0:n],j_x[0:n],j_y[0:n],j_z[0:n])

    free(m);
    free(mu);
    free(m_new);
    free(j_x);
    free(j_y);
    free(j_z);

    return 0;
}
