import numpy as np
from argparse import ArgumentParser
import matplotlib.pyplot as plt

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename", required = True,
                    help="file to plot", metavar="FILE")

args = parser.parse_args()
c = np.loadtxt(args.filename)

#Nx = 4096
#Ny = 4096

Nx = Ny = int(round(np.sqrt(c.size)))

print(Nx, Ny)

c = c.reshape((Nx, Ny))

plt.imshow(c, cmap = 'rainbow')
plt.colorbar()
#plt.show()
plt.savefig('{}.pdf'.format(args.filename))
