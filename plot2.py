import numpy as np
import plotly.graph_objects as go
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename", required = True,
                    help="file to plot", metavar="FILE")

args = parser.parse_args()
c = np.loadtxt(args.filename)

#Nx = 64
#Ny = 1024
#Nz = 1024

Nx = Ny = Nz = int(round(c.size**(1./3.)))

print(Nx)

x = np.array(range(Nx))
y = np.array(range(Ny))
z = np.array(range(Nz))

x, y, z = np.meshgrid(x, y, z)

x = np.ravel(x)
y = np.ravel(y)
z = np.ravel(z)

i1 = np.where(x * y == 0)
i2 = np.where(x * z == 0)
i3 = np.where(y * z == 0)

i = np.union1d(np.union1d(i1, i2), i3)

print(i.size)

x = x[i]
y = y[i]
z = z[i]
c = c[i]

marker_data = go.Scatter3d(
        x=x, 
    y=y, 
    z=z, 
    marker=go.scatter3d.Marker(size=4, color = c, colorscale = 'Rainbow', showscale=True, symbol = 'square'), 
    opacity=1.0, 
    mode='markers'
)

layout = go.Layout(
             scene=dict(
                 aspectmode='data'
         ))


fig=go.Figure(data=marker_data, layout = layout)
fig.show()
